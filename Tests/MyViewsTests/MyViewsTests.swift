import XCTest
@testable import MyViews

final class MyViewsTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(MyViews().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
