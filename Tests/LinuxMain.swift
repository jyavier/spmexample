import XCTest

import MyViewsTests

var tests = [XCTestCaseEntry]()
tests += MyViewsTests.allTests()
XCTMain(tests)
