struct MyViews {
    var text = "Hello, World!"
}

import UIKit
import SwiftUI


public protocol NibReusableSPM {

}
public extension NibReusableSPM {
  public static var nib: UINib { UINib(nibName: String(describing: self), bundle: Bundle.module) }
}


public class ViewExample: UIView, NibReusableSPM {


  public override init(frame: CGRect) {
    super.init(frame: frame)
    xibSetup()
  }

  required init?(coder: NSCoder) {
    super.init(coder: coder)
    xibSetup()
  }

  fileprivate func xibSetup() {
    let newView: UIView = loadViewFromXib()
    newView.translatesAutoresizingMaskIntoConstraints = false
    addSubview(newView)

    let topConstarint: NSLayoutConstraint = NSLayoutConstraint(item: self,
                                                               attribute: NSLayoutConstraint.Attribute.top,
                                                               relatedBy: NSLayoutConstraint.Relation.equal,
                                                               toItem: newView,
                                                               attribute: NSLayoutConstraint.Attribute.top,
                                                               multiplier: 1.0,
                                                               constant: CGFloat.zero)
    let bottomConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self,
                                                                  attribute: NSLayoutConstraint.Attribute.bottom,
                                                                  relatedBy: NSLayoutConstraint.Relation.equal,
                                                                  toItem: newView,
                                                                  attribute: NSLayoutConstraint.Attribute.bottom,
                                                                  multiplier: 1.0,
                                                                  constant: CGFloat.zero)
    let leftConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self,
                                                                attribute: NSLayoutConstraint.Attribute.left,
                                                                relatedBy: NSLayoutConstraint.Relation.equal,
                                                                toItem: newView,
                                                                attribute: NSLayoutConstraint.Attribute.left,
                                                                multiplier: 1.0,
                                                                constant: CGFloat.zero)
    let rightConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self,
                                                                 attribute: NSLayoutConstraint.Attribute.right,
                                                                 relatedBy: NSLayoutConstraint.Relation.equal,
                                                                 toItem: newView,
                                                                 attribute: NSLayoutConstraint.Attribute.right,
                                                                 multiplier: 1.0,
                                                                 constant: CGFloat.zero)
    self.addConstraints([topConstarint,
                         bottomConstraint,
                         leftConstraint,
                         rightConstraint])

  }

  func loadViewFromXib() -> UIView {
    let bundle: Bundle = Bundle.module
    let nib: UINib = UINib(nibName: String(describing: type(of: self)),
                           bundle: bundle)
    guard let view: UIView = nib.instantiate(withOwner: self,
                                             options: nil).first as? UIView else { return UIView() }
    return view
  }

  public override func layoutSubviews() {
    super.layoutSubviews()
    self.backgroundColor = [UIColor.green, UIColor.red, UIColor.purple].randomElement()
  }
}




